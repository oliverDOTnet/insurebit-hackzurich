﻿using InsureBit_HackZurich.UWP.Common;
using InsureBit_HackZurich.UWP.Views;
using Windows.ApplicationModel.Core;
using Windows.Foundation.Metadata;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP
{ 
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.ApplicationView"))
            {
                var titleBar = ApplicationView.GetForCurrentView().TitleBar;
                if (titleBar != null)
                {
                    //titleBar.ButtonBackgroundColor = Color.FromArgb(0, 0, 56, 184);
                    //titleBar.ButtonForegroundColor = Colors.White;
                    //titleBar.BackgroundColor = Color.FromArgb(0, 0, 56, 184);
                    //titleBar.ForegroundColor = Colors.White;

                    titleBar.ButtonBackgroundColor = Colors.Transparent;
                    CoreApplicationViewTitleBar coreTitleBar = CoreApplication.GetCurrentView().TitleBar;
                    coreTitleBar.ExtendViewIntoTitleBar = true;
                    //Window.Current.SetTitleBar(this.titleBar);
                }
            }

            this.rootFrame.Navigate(typeof(WelcomePage), null, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(WelcomePage), null, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_1(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(AccountPage), AccountTargetType.Data, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_2(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(NewInsurancePage), null, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_3(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(ReportClaimPage), null, new DrillInNavigationTransitionInfo());
        }
    }
}
