﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsureBit_HackZurich.UWP.API
{

    public class AXARootobject
    {
        public Links links { get; set; }
        public List<Car> data { get; set; }
    }

    public class Links
    {
        public string cur { get; set; }
        public string first { get; set; }
        public object prev { get; set; }
        public object next { get; set; }
        public string last { get; set; }
        public int count { get; set; }
        public int totalCount { get; set; }
    }

    public class Car
    {
        public string _id { get; set; }
        public int id { get; set; }
        public string brand { get; set; }
        public string model { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public string body { get; set; }
        public int seats { get; set; }
        public int ccm { get; set; }
        public float power_KW { get; set; }
        public float power_PS { get; set; }
        public string gear { get; set; }
        public float noise { get; set; }
        public string fuel { get; set; }
        public float consumption { get; set; }
        public float CO2 { get; set; }
        public string E_class { get; set; }
        public float P_greenhouse { get; set; }
        public float P_noise { get; set; }
        public float P_human { get; set; }
        public float P_nature { get; set; }
        public float P_total { get; set; }
        public string stars { get; set; }
        public int stars_no { get; set; }
        public string E_label { get; set; }
        public int E_label_no { get; set; }
        public string KLASSE { get; set; }
        public string B_DI { get; set; }
        public string _4X4 { get; set; }
        public string DIV { get; set; }
    }

}
