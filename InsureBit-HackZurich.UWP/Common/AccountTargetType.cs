﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsureBit_HackZurich.UWP.Common
{
    public enum AccountTargetType
    {
        Insurances,
        Billing,
        Data
    }
}
