﻿using System;
using InsureBit_HackZurich.UWP.Common;
using System.Xml;
using System.Xml.Linq;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP.Views
{
    public sealed partial class WelcomePage : Page
    {
        public WelcomePage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (App.ClaimInProgress)
            {
                this.claimInProgress.Visibility = Visibility.Visible;
                this.demoButton.Visibility = Visibility.Visible;
            }

            if (App.ClaimDone)
                this.claimDone.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(NewInsurancePage), null, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AccountPage), AccountTargetType.Insurances, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AccountPage), AccountTargetType.Billing, new DrillInNavigationTransitionInfo());
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ReportClaimPage), null, new DrillInNavigationTransitionInfo());
        }

        private async void claimInProgress_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog dialog = new MessageDialog("Your Complain is being processed. We will keep you up-to-date.");
            await dialog.ShowAsync();
        }

        private async void claimDone_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog dialog = new MessageDialog("Your Reimbursement is being transferred to your bank account.");
            await dialog.ShowAsync();
        }

        private void demoButton_Click(object sender, RoutedEventArgs e)
        {
            App.ClaimInProgress = false;
            App.ClaimDone = true;

            var xmdock = CreateToast("1 Claim Reimbursement Ready for Payout");
            var toast = new ToastNotification(xmdock);
            var notify = ToastNotificationManager.CreateToastNotifier();

            notify.Show(toast);

            this.claimInProgress.Visibility = Visibility.Collapsed;
            this.claimDone.Visibility = Visibility.Visible;
            this.demoButton.Visibility = Visibility.Collapsed;
        }

        private Windows.Data.Xml.Dom.XmlDocument CreateToast(string message)
        {
            var xDoc = new XDocument(

                new XElement("toast",

                new XElement("visual",

                new XElement("binding", new XAttribute("template", "ToastGeneric"),

                new XElement("text", "InsureBit"),

                new XElement("text", message)

                )

                //),// actions

                //new XElement("actions",

                //new XElement("action", new XAttribute("activationType", "background"),

                //new XAttribute("content", "Yes"), new XAttribute("arguments", "yes")),

                //new XElement("action", new XAttribute("activationType", "background"),

                //new XAttribute("content", "No"), new XAttribute("arguments", "no"))

                )

                )

            );

            var xmlDoc = new Windows.Data.Xml.Dom.XmlDocument();
            xmlDoc.LoadXml(xDoc.ToString());

            return xmlDoc;
        }
    }
}
