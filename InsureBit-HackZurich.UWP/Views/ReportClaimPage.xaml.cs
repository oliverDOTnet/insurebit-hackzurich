﻿using InsureBit_HackZurich.UWP.API;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP.Views
{
    public sealed partial class ReportClaimPage : Page
    {
        private AXARootobject carData;
        private bool setContract = false;
        private bool setDescription = true;
        private bool setHappenedOn = true;
        private bool setHappenedAt = true;

        public ReportClaimPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            this.contractBox.Items.Add(App.NewInsurance);
            this.happendedDatePicker.Date = DateTime.Today;
        }

        private void enableReport()
        {
            this.reportButton.IsEnabled = this.setContract && this.setDescription && this.setHappenedAt && this.setHappenedOn;
        }

        private void contractBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                this.setContract = true;
                enableReport();
            }
        }

        private void happendedDatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            if (this.happendedDatePicker.Date.HasValue)
            {
                this.setHappenedOn = true;
                enableReport();
            }
        }

        private async void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;

            while (this.progressBar.Value < 100)
            {
                this.progressBar.Value++;
                await Task.Delay(15);
            }
        }

        private async void reportButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.progressRing.IsActive = true;

            await Task.Delay(1500);

            this.progressRing.IsActive = false;

            App.ClaimInProgress = true;
            App.ClaimDone = false;
            App.ClaimTitle = "Claimed - " + App.NewInsurance;

            this.Frame.Navigate(typeof(WelcomePage), null, new DrillInNavigationTransitionInfo());
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.setDescription = true;
            enableReport();
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            this.setHappenedAt = true;
            enableReport();
        }
    }
}
