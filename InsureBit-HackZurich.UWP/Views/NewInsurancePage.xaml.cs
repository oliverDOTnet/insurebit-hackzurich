﻿using InsureBit_HackZurich.UWP.API;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP.Views
{
    public sealed partial class NewInsurancePage : Page
    {
        private AXARootobject carData;

        private bool setBrand = false;
        private bool setModel = false;
        private bool setRegDate = true;

        private Car selectedModel;

        public NewInsurancePage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            this.regDatePicker.Date = DateTime.Today.AddMonths(-1);

            HttpClient client = new HttpClient();

            string serverResponseString = await client.GetStringAsync("http://localhost:3000/axa/cars?skip=0&limit=5000");

            carData = JsonConvert.DeserializeObject<AXARootobject>(serverResponseString);

            this.brandNameBox.ItemsSource = carData.data.GroupBy(s => s.brand).OrderBy(g => g.Key);

            //for (int i = 2016; i > 1980; i--)
            //{
            //    this.regDateBox.Items.Add(i);
            //}

            //this.regDateBox.Items.Add(1950);
            //this.regDateBox.Items.Add(1940);
            //this.regDateBox.Items.Add(1930);
            //this.regDateBox.Items.Add(1920);
            //this.regDateBox.Items.Add(1910);
            //this.regDateBox.Items.Add(1900);
        }

        private void brandNameBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                this.modelNameBox.ItemsSource = carData.data.Where(c => c.brand.Equals((this.brandNameBox.SelectedItem as IGrouping<string, Car>).Key)).OrderBy(g => g.model);
                this.setBrand = true;
                enableSubmit();
            }
        }

        private void modelNameBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                this.selectedModel = this.modelNameBox.SelectedItem as Car;
                this.setModel = true;
                enableSubmit();
            }
        }

        private void regDatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            if (this.regDatePicker.Date.HasValue)
            {
                this.setRegDate = true;
                enableSubmit();
            }
        }

        private void enableSubmit()
        {
            this.submitButton.IsEnabled = this.setBrand && this.setModel && this.setRegDate;
        }

        private async void submitButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.progressRing.IsActive = true;

            await Task.Delay(1500);

            this.progressRing.IsActive = false;

            this.Frame.Navigate(typeof(InsuranceAcceptedPage), this.selectedModel, new DrillInNavigationTransitionInfo());
        }

        private async void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.progressBar.Visibility = Windows.UI.Xaml.Visibility.Visible;

            while (this.progressBar.Value < 100)
            {
                this.progressBar.Value++;
                await Task.Delay(15);
            }
        }
    }
}
