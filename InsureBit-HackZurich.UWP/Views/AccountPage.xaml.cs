﻿using InsureBit_HackZurich.UWP.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP.Views
{
    public sealed partial class AccountPage : Page
    {
        public AccountPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            //for (int i = 0; i < 20; i++)
            //{
            //    this.insuranceListView.Items.Add("insurance" + i);
            //    this.billingListView.Items.Add("bill" + i);
            //    this.dataListView.Items.Add("data" + i);
            //}

            this.insuranceListView.Items.Add(App.NewInsurance);
            this.billingListView.Items.Add("Receipt 09/16 - " + App.NewInsurance);

            for (int i = 0; i < 1000; i++)
            {
                this.billingListView.Items.Add(" > " + Guid.NewGuid());
            }

            this.dataListView.Items.Add("Alice Chataway");
            this.dataListView.Items.Add("Friedrichstr. 189");
            this.dataListView.Items.Add("104053 Berlin");
            this.dataListView.Items.Add("02/08/1986");
            this.dataListView.Items.Add("Germany");
            this.dataListView.Items.Add("DE85 4669 103 592");
            this.dataListView.Items.Add("BYLADEM1001");

            AccountTargetType target = (AccountTargetType)e.Parameter;

            switch (target)
            {
                case AccountTargetType.Insurances:
                    this.rootPivot.SelectedIndex = 0;
                    break;
                case AccountTargetType.Billing:
                    this.rootPivot.SelectedIndex = 1;
                    break;
                case AccountTargetType.Data:
                    this.rootPivot.SelectedIndex = 2;
                    break;
                default:
                    break;
            }
        }
    }
}
