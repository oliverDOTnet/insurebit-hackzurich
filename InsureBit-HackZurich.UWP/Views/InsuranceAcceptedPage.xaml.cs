﻿using InsureBit_HackZurich.UWP.API;
using InsureBit_HackZurich.UWP.Common;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace InsureBit_HackZurich.UWP.Views
{
    public sealed partial class InsuranceAcceptedPage : Page
    {
        private Car selectedModel;

        public InsuranceAcceptedPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Car model = e.Parameter as Car;
            this.selectedModel = model;
            this.text1.Text = this.text1.Text + this.selectedModel?.name + ":";
            this.offerText.Text = (((20 / (int)this.selectedModel.P_total) + 1) * 50) + ".39€";
        }

        private async void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.progressRing.IsActive = true;

            await Task.Delay(1500);

            this.progressRing.IsActive = false;

            App.NewInsurance = "InsureBit Car - " + this.selectedModel.name;

            this.Frame.Navigate(typeof(AccountPage), AccountTargetType.Insurances, new DrillInNavigationTransitionInfo());
        }
    }
}
